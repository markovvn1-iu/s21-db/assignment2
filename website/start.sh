#!/bin/bash

HOME_PATH="$(dirname "$(realpath "$0")")"

docker run -it --rm -v "${HOME_PATH}/data:/www/data" -p 8080:80 assignment2_website
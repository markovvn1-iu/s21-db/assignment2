#!/bin/bash

#/docker-entrypoint.sh postgres
/docker-entrypoint-init-only.sh postgres > /dev/null 2> /dev/null
su - postgres -c "/usr/lib/postgresql/13/bin/pg_ctl -D /var/lib/postgresql/data start"
#mkdir -p /output
pg_dump -U postgres dvdrental > /output/dvdrental_dump.sql
su - postgres -c "/usr/lib/postgresql/13/bin/pg_ctl -D /var/lib/postgresql/data stop"